<?php

/**
 * Admin settings form.
 */
function qbo_api_admin_settings($form, &$form_state) {
  module_load_install('qbo_api');

  // Requirements.
  $form['requirements'] = array(
    '#type' => 'fieldset',
    '#title' => t('Requirements'),
  );
  $form['requirements']['report'] = array(
    '#markup' => theme('status_report', array('requirements' => qbo_api_requirements('runtime'))),
  );

  // Configuration.
  $qbo = new QBO();
  $qbo->configForm($form, $form_state);

  return $form;
}

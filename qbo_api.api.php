<?php

/**
 * Implements hook_qbo_api_contexts().
 *
 * QBO Contexts allow different types of contextual connections to the
 * QBO API. This module provides the QBOContextGlobal which is a global,
 * site-wide connection (configurable via the admin interface).
 *
 * Other examples could be node-based, user-based, etc.
 *
 * See the QBOContextGlobal.inc file for an example of the class implementation.
 *
 * In order to provide a fully-functional context, extra module code is
 * required (menu entries, permissions, form implementations, etc).
 *
 * @return
 *   An array of available QBOContext class implementations keyed by class
 *   name with a value of the path of the class file, which will be loaded
 *   automatically when needed.
 */
function hook_qbo_api_contexts() {
  return array(
    'QBOContextGlobal' => drupal_get_path('module', 'qbo_api') . '/includes/QBOContextGlobal.inc',
  );
}

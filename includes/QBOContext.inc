<?php

/**
 * Abstract class to define QBO contextual connections.
 */
abstract class QBOContext {

  /**
   * Constructor.
   *
   * @param $args
   *   An array of arguments passed in to the constructor.
   */
  public function __construct($args = array()) {

  }

  /**
   * Generate a unique ID for this context based on any optional arugments
   * passed in during creation.
   *
   * @return
   *   A unique ID.
   */
  abstract public function id();

  /**
   * Return a set of QBO keys for this context.
   *
   * @return
   *   An array of QBO keys, or NULL if there are none.
   */
  abstract public function keys();

  /**
   * Save keys/configuration.
   *
   * @param $values
   *   An array of keys or values from the config form.
   */
  abstract public function save($values);

  /**
   * Disconnect from QBO, which essentially means dropping the OAuth keys.
   */
  abstract public function disconnect();

  /**
   * Determine if the current user has access to connect to QBO.
   *
   * This is used in the OAuth page callback. If you require more
   * access control, then you must invoke this yourself.
   *
   * @return
   *   TRUE if the user has access, otherwise FALSE.
   */
  abstract public function access();

  /**
   * Return an array of all available sets of keys along with related
   * context arguments.
   *
   * This is invoked by hook_cron() in order to determine which sets
   * of keys need to be updated.
   *
   * @return
   *   An array of context key data. The context should return an array
   *   for every set of available credentials regardless of which keys
   *   are set and what the auth time is. The API module will determine
   *   which need to be updated. The format should be as follows:
   *     $data[] = array(
   *       'args' => array('nid' => 2),
   *       'keys' => array(
   *         'company_id' => 12345,
   *         'consumer_token' => 4311,
   *         etc..
   *       ),
   *     );
   *     $data[] = array(etc..);
   */
  abstract public static function allKeys();

  /**
   * Define the required QBO key names.
   *
   * @return
   *   An array of key names.
   */
  public static function keyNames() {
    return array(
      'company_id',
      'consumer_key',
      'consumer_secret',
      'access_token',
      'access_secret',
      'auth_date',
    );
  }

  /**
   * Filter an array, keeping only those keys which are mapped in
   * keyNames().
   *
   * @param $values
   *   An associative array.
   * @return
   *   The argument array, keeping only those keys that match keyNames().
   */
  public static function filterKeyValues($values) {
    foreach ($values as $key => $value) {
      if (!in_array($key, self::keyNames())) {
        unset($values[$key]);
      }
    }
    return $values;
  }
}

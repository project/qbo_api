<?php

/**
 * Quickbooks Online API connection class.
 */
class QBO {

  /**
   * QBO context.
   */
  public $context = NULL;

  /**
   * QBO credential keys.
   */
  public $keys = array();

  /**
   * QBO DataService.
   */
  private $dataService = NULL;

  /**
   * QBO PlatformService.
   */
  private $platformService = NULL;

  /**
   * Drupal OAuth URL.
   */
  private $oathUrl = NULL;

  /**
   * Query size per page.
   */
  const QUERY_PAGE_SIZE = 500;

  /**
   * Constructor.
   *
   * @param $context
   *   The name of the context class to use for the connection.
   * @param $args
   *   An optional array of arguments to pass to the context.
   */
  public function __construct($context = 'QBOContextGlobal', $args = array()) {
    // Load the library.
    libraries_load('qbo');

    // Load the context and make sure the class exists.
    if (!qbo_api_load_context($context) || !class_exists($context)) {
      throw new QBOException('Undefined QBO context: ' . $context);
    }

    // Initialize the context.
    $this->context = new $context($args);

    // Load the credential keys.
    $this->updateKeys();

    // Set the Drupal OAuth URL.
    $this->oauthUrl = url('qbo/oauth', array(
      'query' => array(
        'context' => $context,
        'args' => $args,
      ),
    ));
  }

  /**
   * Return the credential keys, if valid.
   *
   * Use this function instead of directly accessing the variable if
   * you only want a full set of keys to be returned.
   *
   * @return
   *   An array of credential keys, or NULL if none are available.
   */
  public function keys() {
    return (!empty($this->keys) && count(array_filter($this->keys)) == count($this->context->keyNames())) ? $this->keys : NULL;
  }

  /**
   * Update the keys from the context; not QBO.
   */
  public function updateKeys() {
    // Update the keys and merge in defaults.
    return $this->keys = array_merge(array(
      'company_id' => NULL,
      'consumer_key' => NULL,
      'consumer_secret' => NULL,
      'access_token' => NULL,
      'access_secret' => NULL,
      'auth_date' => NULL,
    ), $this->context->keys());
  }

  /**
   * Create a QBO API DataService class.
   *
   * @return DataService
   *   A QBO API Data Service client class.
   */
  public function dataService() {
    // Return if already initialized.
    if ($this->dataService) {
      return $this->dataService;
    }

    // Create the service.
    $service = $this->service();

    // Disable logging.
    $service->IppConfiguration->Logger->RequestLog->EnableRequestResponseLogging = FALSE;

    // Initialize the Data Service.
    return $this->dataService = new DataService($service);
  }

  /**
   * Create a QBO API PlatformService class.
   *
   * @return PlatformService
   *   A QBO API Platform Service client class.
   */
  public function platformService() {
    // Return if already initialized.
    if ($this->platformService) {
      return $this->platformService;
    }

    // Initialize the Platform Service.
    return $this->platformService = new PlatformService($this->service());
  }

  /**
   * Create a service context used to create service classes.
   *
   * @return
   *   A ServiceContext class.
   */
  private function service() {
    // Validate the keys.
    if (!$this->keys()) {
      throw new QBOException('Missing or invalid QBO credentials.');
    }

    // Authenticate via OAuth and create a Data Service object.
    $request_validator = new OAuthRequestValidator($this->keys['access_token'], $this->keys['access_secret'], $this->keys['consumer_key'], $this->keys['consumer_secret']);
    $service_context = new ServiceContext($this->keys['company_id'], IntuitServicesType::QBO, $request_validator);

    // Check that the service context was created.
    if (!$service_context) {
      throw new QBOException('Could not initialize a service context. Verify your consumer and access keys are valid.');
    }

    return $service_context;
  }

  /**
   * Disconnect from QBO OAuth.
   *
   * This is essentially a wrapper for PlatformService::Disconnect but
   * we require more to take place during a disconnect.
   *
   * There is no return value because the only thing that matters is
   * that we drop the OAuth keys on our end.
   */
  public function disconnect() {
    // Disconnect from QBO.
    $this->platformService()->Disconnect();

    // Tell the context to disconnect their OAuth keys.
    $this->context->disconnect();
  }

  /**
   * Reconnect to QBO OAuth.
   *
   * This is essentially a wrapper for PlatformService::Reconnect but
   * we require more to take place during a reconnect.
   *
   * @return
   *   TRUE if the reconnet was successful, otherwise FALSE.
   */
  public function reconnect() {
    // Reconnect to QBO.
    $response = $this->platformService()->Reconnect();

    // Evalute success.
    if ($success = $response->ErrorCode == '0') {
      // Save the keys.
      $this->context->save(array(
        'access_token' => $response->OAuthToken,
        'access_secret' => $response->OAuthTokenSecret,
        'auth_date' => REQUEST_TIME,
      ));
    }
    else {
      watchdog('qbo_api', 'Reconnect failed (Context: !context): !msg', array(
        '!context' => $this->context->id(),
        '!msg' => $response->ErrorMessage,
      ), WATCHDOG_ERROR);
    }

    return $success;
  }

  /**
   * Perform a query against the Quickbooks API.
   *
   * Responses will be cached both statically and in the database.
   *
   * @param $query
   *   The SQL query to perform.
   * @param $reset
   *   TRUE if the cache should be bypassed. Defaults to FALSE.
   *
   * @return
   *   The query response, or NULL if nothing was returned or the call
   *   failed.
   */
  public function query($query, $reset = FALSE) {
    $responses = &drupal_static('qboQueries', array());

    // Create a key for this query.
    $key = 'qbo:query:' . $this->context->id() . ':' . md5(strtolower($query));

    // Check the static cache.
    if (!isset($responses[$key]) || $reset) {
      // Initialize the static cache.
      $responses[$key] = array();

      // Check the database cache.
      if (!$reset && ($cache = cache_get($key))) {
        // Extract the cached data.
        $responses[$key] = $cache->data;
      }
      else {
        // Perform the query, using pages if the result sets are larger
        // than the defined page size. Unfortauntely Query() is broken
        // and the page and page size parameters do not work. We will
        // use the query itself to handle page and page size instead.
        // @see https://goo.gl/XHuNWu
        $start = 1;
        while (TRUE) {
          // Generate a query with proper range.
          $range_query = $query . " STARTPOSITION {$start} MAXRESULTS " . self::QUERY_PAGE_SIZE;

          // Fetch the results.
          $response = $this->dataService()->Query($range_query);

          // Increment the range for the next potential run.
          $start += self::QUERY_PAGE_SIZE;

          // See if results were returned.
          if (is_array($response)) {
            // Add the response to the overall results, if something was
            // returned.
            $responses[$key] = array_merge($responses[$key], $response);

            // If the size of the response is the size of the page, we
            // need to move on to the next page.
            if (count($response) == self::QUERY_PAGE_SIZE) {
              continue;
            }
          }

          // Stop the looping.
          break;
        }

        // Cache the data for next time, if data was returned.
        if ($responses[$key]) {
          cache_set($key, $responses[$key], 'cache', CACHE_TEMPORARY);
        }
      }
    }

    return $responses[$key];
  }

  /**
   * Convert query results to an array suitable to be used as form element
   * options.
   *
   * @param $query
   *   The SQL query to pass to the QBO API. The query must return at least
   *   the Id and Name.
   * @param $empty
   *   An untranslated string to be used as the optional empty value which
   *   resides at the top of the options list. This defaults to "- Select one
   *   -". If set to NULL or FALSE, this will not be used.
   * @param $reset
   *   TRUE if the query cache should be reset.
   *
   * @return
   *   An array of results from the query keyed by ID with the name as the
   *   value.
   */
  public function queryAsOptions($query, $empty = '- Select one -', $reset = FALSE) {
    $options = array();

    // Add an empty option to the top, if requested.
    if ($empty) {
      $options[''] = t($empty);
    }

    // Execute the query.
    if ($results = $this->query($query, $reset)) {
      // Iterate the results.
      foreach ($results as $result) {
        $options[$result->Id] = $result->Name;
      }
    }

    return $options;
  }

  /**
   * Helper function to query for an object by Id.
   *
   * @param $type
   *   The object type (ie, SalesReceipt, JournalEntry, Item, Account).
   * @param $id
   *   The object Id, which must be numeric.
   * @param $reset
   *   TRUE if the cache should be bypassed, otherwise FALSE. Defaults to
   *   FALSE.
   *
   * @return
   *   The fetched object, if found, otherwise NULL.
   */
  public function getObject($type, $id, $reset = FALSE) {
    // Execute the query.
    if ($results = $this->query("SELECT * FROM {$type} WHERE Id = '{$id}'", $reset)) {
      // Only one item should be available.
      return isset($results[0]) ? $results[0] : NULL;
    }

    return NULL;
  }

  /**
   * Provide a configuration form for QBO connections.
   *
   * @param &$form
   *   A form array.
   * @param &$form_state
   *   A form state array.
   */
  public function configForm(&$form, &$form_state) {
    // Store the QBO object.
    $form['#qbo'] = $this;

    // Status.
    $form['status'] = array(
      '#type' => 'fieldset',
      '#title' => t('Status'),
    );
    $form['status']['report'] = array(
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => array(
        'style' => 'font-weight: bold; color: ' . ($this->keys() ? 'green' : 'red') . ';',
      ),
      '#value' => $this->keys() ? t('Connected') : t('Disconnected'),
    );

    // Connection.
    $form['connection'] = array(
      '#type' => 'fieldset',
      '#title' => t('Connection'),
    );
    $form['connection']['help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Help'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['connection']['help']['list'] = array(
      '#theme' => 'item_list',
      '#type' => 'ol',
      '#items' => array(
        t('Log in to <a href="@url" target="_blank">Quickbooks Online</a>.', array('@url' => 'https://qbo.intuit.com')),
        t('Click the gear icon in the top-right. Select "Account and Settings".'),
        t('Click on "Billing & Subscription".'),
        t('Copy the "Company ID" into this form.'),
        t('Sign into <a href="@url" target="_blank">developer.intuit.com</a> (use same email as your Quickbooks Online account).', array('@url' => 'https://developer.intuit.com')),
        t('Select "My apps", then "Create new app". Choose "Select APIs" in the "Just Start Coding" section.'),
        t('Select "Accounting (and/or Payments)", then "Create app".'),
        t('Navigate to "QuickBooks sandbox" under the Resources section. Add a sandbox company (there may already be a default company).'),
        t('Go back to "My apps", copy the "oAuth Consumer" and "oAuth Consumer Secret" tokens under the "Keys" tab into this form and click Save.'),
        t('Navigate to <a href="@url" target="_blank">the oAuth page</a> and follow the instructions to generate and save your Access and Access Secret tokens.', array('@url' => url("admin/config/services/qbo/activate"))),
      ),
    );
    $form['connection']['company_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Company ID'),
      '#required' => TRUE,
      '#default_value' => $this->keys['company_id'],
    );
    $form['connection']['consumer_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer key'),
      '#required' => TRUE,
      '#default_value' => $this->keys['consumer_key'],
    );
    $form['connection']['consumer_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer secret'),
      '#required' => TRUE,
      '#default_value' => $this->keys['consumer_secret'],
    );
    $form['connection']['oauth'] = array(
      '#type' => 'fieldset',
      '#title' => t('OAuth connection'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#access' => (bool) ($this->keys['company_id'] && $this->keys['consumer_key'] && $this->keys['consumer_secret']),
    );
    $form['connection']['oauth']['button'] = array(
      '#access' => (bool) !$this->keys['access_token'],
      '#markup' => '<ipp:connectToIntuit></ipp:connectToIntuit>',
      '#attached' => array(
        'js' => array(
          'https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js' => array(
            'type' => 'external',
            'weight' => 1,
          ),
          drupal_get_path('module', 'qbo_api') . '/js/qbo_api.js' => array(
            'weight' => 2,
          ),
          array(
            'type' => 'setting',
            'data' => array(
              'qboApi' => array(
                'oauthUrl' => substr($this->oauthUrl, 1),
              ),
            ),
          ),
        ),
      ),
    );
    $form['connection']['oauth']['reconnect'] = array(
      '#type' => 'submit',
      '#value' => t('Reconnect'),
      '#limit_validation_errors' => array(),
      '#submit' => array('qbo_api_config_form_oauth_reconnect'),
      '#access' => (bool) $this->keys['access_token'] && qbo_api_token_needs_refresh($this->keys['auth_date']),
    );
    $form['connection']['oauth']['disconnect'] = array(
      '#type' => 'submit',
      '#value' => t('Disconnect'),
      '#limit_validation_errors' => array(),
      '#submit' => array('qbo_api_config_form_oauth_disconnect'),
      '#access' => (bool) $this->keys['access_token'],
    );
    $form['connection']['oauth']['tokens'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tokens'),
      '#description' => !$this->keys() ? t('The tokens below will be automatically populated after connecting using the button above.') : '',
    );
    $form['connection']['oauth']['tokens']['access_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Access token'),
      '#required' => FALSE,
      '#default_value' => $this->keys['access_token'],
    );
    $form['connection']['oauth']['tokens']['access_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Access token secret'),
      '#required' => FALSE,
      '#default_value' => $this->keys['access_secret'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#submit' => array('qbo_api_config_form_save'),
    );
  }
}

/**
 * Custom Exception class for QBO.
 */
class QBOException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct($message, $code = 0, Exception $previous = NULL) {
    // Log the message.
    watchdog('qbo_api', $message, array(), WATCHDOG_ERROR);

    parent::__construct($message, $code, $previous);
  }
}

/**
 * Form submit handler for the config form.
 */
function qbo_api_config_form_save($form, &$form_state) {
  // Trigger the context.
  $form_state['values']['company_id'] = str_replace(' ', '', $form_state['values']['company_id']);
  $form['#qbo']->context->save($form_state['values']);
}

/**
 * Form submit handler for the config form OAuth disconnect.
 */
function qbo_api_config_form_oauth_disconnect($form, &$form_state) {
  try {
    // Disconnect from QBO.
    $form['#qbo']->disconnect();
  }
  catch (QBOException $e) {
    // We don't really care if creds are missing, invalid, or if this
    // call failed, because the main point of disconnecting is dropping
    // the keys, which the context plugin will do below.
  }

  // Alert the user.
  drupal_set_message(t('Disconnect successful.'));
}

/**
 * Form submit handler for the config form OAuth reconnect.
 */
function qbo_api_config_form_oauth_reconnect($form, &$form_state) {
  try {
    // Reconnect to QBO.
    $success = $form['#qbo']->reconnect();
  }
  catch (QBOException $e) {
    $success = FALSE;
  }

  // Evaluate success.
  if ($success) {
    drupal_set_message(t('Reconnect successful.'));
  }
  else {
    drupal_set_message(t('Reconnect failed.'), 'error');
  }
}

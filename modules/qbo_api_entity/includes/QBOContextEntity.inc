<?php

/**
 * QBO contextual for entity connections.
 */
class QBOContextEntity extends QBOContext {

  /**
   * The entity ID.
   */
  private $entity_id = NULL;
  
  /**
   * The entity type.
   */
  private $entity_type = NULL;

  /**
   * Constructor.
   */
  public function __construct($args) {
    // Make sure an entity ID and type is present.
    if (empty($args['entity_id']) || !is_numeric($args['entity_id']) || empty($args['entity_type'])) {
      throw new QBOException('Missing or invalid entity ID passed to context.');
    }

    // Store the entity ID and type.
    $this->entity_id = $args['entity_id'];    
    $this->entity_type = $args['entity_type'];    

    // Validate the entity can be loaded.
    if (!$this->entity()) {
      throw new QBOException('Unable to load entity passed to context.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->entity_type . ':' . $this->entity_id;
  }

  /**
   * {@inheritdoc}
   */
  public function keys() {
    $keys = &drupal_static('qboEntityContextKeys', array());

    // Check if the keys are present for this entity.
    if (!isset($keys[$this->id()])) {
      // Query to find the keys.
      $results = db_select('qbo_api_entity', 'e')
        ->fields('e', array('qbo'))
        ->condition('e.entity_id', $this->entity_id)
        ->condition('e.entity_type', $this->entity_type)
        ->execute()
        ->fetchField();

      // Unserialize if available.
      $keys[$this->id()] = !empty($results) ? unserialize($results) : array();
    }

    return $keys[$this->id()];
  }

  /**
   * {@inheritdoc}
   */
  public static function allKeys() {
    // To avoid duplicate keys being returned, classes implementing this
    // class should select the keys pertaining to that entity type.
  }

  /**
   * {@inheritdoc}
   */
  public function save($values) {
    // Load the existing keys and merge in the new values.
    $keys = array_merge($this->keys(), $this->filterKeyValues($values));

    // Save the keys.
    db_merge('qbo_api_entity')
      ->key(array(
        'entity_id' => $this->entity_id,
        'entity_type' => $this->entity_type,
      ))
      ->fields(array(
        'entity_id' => $this->entity_id,
        'entity_type' => $this->entity_type,
        'qbo' => serialize($keys),
      ))
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function disconnect() {
    // Check if keys are stored.
    if ($this->keys()) {
      // Delete OAuth data.
      $this->save(array(
        'access_token' => FALSE,
        'access_secret' => FALSE,
        'auth_date' => FALSE,
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    global $user;

    // Clone the user.
    $account = clone $user;

    // Check for an entity access callback.
    if (($info = entity_get_info()) && isset($info[$this->entity_type]['access callback'])) {
      return $info[$entity_type]['access callback']('update', $this->entity(), $account, $this->entity_type);
    }

    // Default to site-wide permissions.
    return user_access('administer qbo api', $account);
  }

  /**
   * Load and return the entity.
   *
   * @return
   *   An entity object, otherwise NULL if not available.
   */
  public function entity() {
    $entities = entity_load($this->entity_type, array($this->entity_id));
    return reset($entities);
  }
}

<?php

/**
 * QBO contextual for user connections.
 */
class QBOContextUser extends QBOContextEntity {

  /**
   * Constructor.
   */
  public function __construct($args) {
    // Make sure a user ID is present.
    if (empty($args['uid']) || !is_numeric($args['uid'])) {
      throw new QBOException('Missing or invalid user ID passed to context.');
    }

    parent::__construct(array('entity_type' => 'user', 'entity_id' => $args['uid']));
  }

  /**
   * {@inheritdoc}
   */
  public static function allKeys() {
    $data = array();

    // Query to find all sets of user credential keys.
    $rows = db_select('qbo_api_entity', 'e')
      ->fields('e')
      ->condition('e.entity_type', 'user')
      ->execute()
      ->fetchAll();

    // Iterate the rows.
    foreach ($rows as $row) {
      // Add to the set to return.
      $data[] = array(
        'args' => array('uid' => $row->entity_id),
        'keys' => unserialize($row->qbo),
      );
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    global $user;

    // Load the user.
    if ($account = $this->entity()) {
      // Check for the permission to edit all users.
      if (user_access('edit all qbo users')) {
        return TRUE;
      }

      // Check for the permission to edit yourself.
      if (user_access('edit own qbo user')) {
        // Check that the user is the current user.
        if ($account->uid == $user->uid) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }
}

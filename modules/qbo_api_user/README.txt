This module provides user-based connections for the Quickbooks Online API.

To use this module:
 1. Enable the module.
 2. Set user permissions.
 3. When viewing a user, there will be a Quickbooks tab where you can
    connect the user to Quickbooks.

Please see the project page for general documentation and examples.

To spawn a QBO class for a given user, do the following:

$qbo = new QBO('QBOContextUser', array('uid' => $user->uid));

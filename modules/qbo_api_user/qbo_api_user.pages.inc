<?php

/**
 * User QBO settings form.
 */
function qbo_api_user_form($form, &$form_state, $account) {
  // Configuration.
  $qbo = new QBO('QBOContextUser', array('uid' => $account->uid));
  $qbo->configForm($form, $form_state);

  return $form;
}

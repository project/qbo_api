<?php

/**
 * Admin settings form.
 */
function qbo_api_node_admin_settings($form, &$form_state) {
  // Generate a list of node types and their default settings.
  $types = $defaults = array();
  foreach (node_type_get_types() as $type) {
    $types[$type->type] = $type->name;
    $defaults[$type->type] = qbo_api_node_enabled($type->type) ? $type->type : NULL;
  }

  $form['types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('QBO-enabled node types'),
    '#options' => $types,
    '#default_value' => $defaults,
    '#description' => t('Select which node types can have QBO connections. If enabled, users with access will see a tab when viewing the node where they can connect to QBO.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Admin settings form submit handler.
 */
function qbo_api_node_admin_settings_submit($form, &$form_state) {
  // Iterate the node types.
  foreach ($form_state['values']['types'] as $type => $enabled) {
    // Generate a variable name for the node type.
    $var = "qbo_api_node_type_{$type}";

    // Check if this node type is enabled.
    if ($enabled) {
      // Save the variable.
      variable_set($var, 1);
    }
    else {
      // Delete the variable since disabled is the default.
      variable_del($var);
    }
  }

  drupal_set_message(t('Settings saved successfully.'));
}

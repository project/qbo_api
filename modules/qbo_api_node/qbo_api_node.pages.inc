<?php

/**
 * Node QBO settings form.
 */
function qbo_api_node_form($form, &$form_state, $node) {
  // Configuration.
  $qbo = new QBO('QBOContextNode', array('nid' => $node->nid));
  $qbo->configForm($form, $form_state);

  return $form;
}

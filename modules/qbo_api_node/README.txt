This module provides node-based connections for the Quickbooks Online API.

To use this module:
 1. Enable the module.
 2. Navigate to admin/config/services/qbo/node.
 3. Choose the node types that can have QBO connections.
 4. Set user permissions.
 5. When viewing a node of an enabled type, there will be a Quickbooks
    tab where you can connect the node to Quickbooks.

Please see the project page for general documentation and examples.

To spawn a QBO class for a given node, do the following:

$qbo = new QBO('QBOContextNode', array('nid' => $node->nid));

<?php

/**
 * QBO contextual for node connections.
 */
class QBOContextNode extends QBOContextEntity {

  /**
   * Constructor.
   */
  public function __construct($args) {
    // Make sure a node ID is present.
    if (empty($args['nid']) || !is_numeric($args['nid'])) {
      throw new QBOException('Missing or invalid node ID passed to context.');
    }

    parent::__construct(array('entity_type' => 'node', 'entity_id' => $args['nid']));
  }

  /**
   * {@inheritdoc}
   */
  public static function allKeys() {
    $data = array();

    // Query to find all sets of node credential keys.
    $rows = db_select('qbo_api_entity', 'n')
      ->condition('n.entity_type', 'node')
      ->fields('n')
      ->execute()
      ->fetchAll();

    // Iterate the rows.
    foreach ($rows as $row) {
      // Add to the set to return.
      $data[] = array(
        'args' => array('nid' => $row->entity_id),
        'keys' => unserialize($row->qbo),
      );
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    // Load the node.
    if ($node = $this->entity()) {
      // Make sure the type is still enabled.
      if (qbo_api_node_enabled($node->type)) {
        // Check for the permission to edit all nodes.
        if (user_access('edit all qbo nodes')) {
          return TRUE;
        }

        // Check for the permission to edit own nodes.
        if (user_access('edit own qbo nodes')) {
          // With this permission the user must also be able to edit
          // the node.
          if (node_access('update', $node)) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }
}

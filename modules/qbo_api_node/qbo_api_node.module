<?php

/**
 * Implements hook_menu().
 */
function qbo_api_node_menu() {
  $items = array();
  $items['node/%node/qbo'] = array(
    'title' => 'Quickbooks',
    'description' => 'Connect this node to Quickbooks Online.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qbo_api_node_form', 1),
    'access callback' => 'qbo_api_node_form_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'qbo_api_node.pages.inc',
  );
  $items['admin/config/services/qbo/node'] = array(
    'title' => 'Nodes',
    'description' => 'Configure the Quickbooks Online node-based connections.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qbo_api_node_admin_settings'),
    'access arguments' => array('administer qbo api'),
    'file' => 'qbo_api_node.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function qbo_api_node_permission() {
  $perms = array();
  $perms['edit all qbo nodes'] = array(
    'title' => t('Edit all credetials'),
    'description' => t('Edit the credentials for all QBO-enabled nodes.'),
    'restrict access' => TRUE,
  );
  $perms['edit own qbo nodes'] = array(
    'title' => t('Edit own credentials'),
    'description' => t('Edit the credentials for all QBO-enabled nodes which the user has access to edit.'),
    'restrict access' => TRUE,
  );
  return $perms;
}

/**
 * Implements hook_admin_paths().
 */
function qbo_api_node_admin_paths() {
  $paths = array(
    'node/*/qbo' => TRUE,
  );
  return $paths;
}

/**
 * Implements hook_qbo_api_contexts().
 */
function qbo_api_node_qbo_api_contexts() {
  return array(
    'QBOContextNode' => drupal_get_path('module', 'qbo_api_node') . '/includes/QBOContextNode.inc',
  );
}

/**
 * Form access callback for QBO node forms.
 */
function qbo_api_node_form_access($node) {
  $qbo = new QBO('QBOContextNode', array('nid' => $node->nid));
  return $qbo->context->access();
}

/**
 * Determine if a given node-type is enabled for QBO connections.
 *
 * @param $type
 *   The node type machine-name.
 * @return
 *   TRUE if the node type is enabled, otherwise FALSE.
 */
function qbo_api_node_enabled($type) {
  return (bool) variable_get("qbo_api_node_type_{$type}", FALSE);
}

Refer to the project page for installation and usage instructions.

See the QBO class inside includes/QBO.inc for available functions.

See the qbo_api_node module inside modules/qbo_api_node for an example
of how to implement a QBO contextual connection.
